name: Penetration Testing Overview
acronym: PTO
satisfies:
  TSC:
    - CC4.1
majorRevisions:
  - date: Jun 1 2020
    comment: Initial document
---

# Procedure

a. We use a third party tool called Detectify to perform the following security scans on our domain and sub-domains. During the Penetration/Exploitation phase Detectify authenticates into an instance of the MentorcliQ platform and utilizes the information gathered in the previous phases to perform extensive tests using known penetration testing methods:

    i. Asset Monitoring
        1. Exposed subdomains, private git repositories, and open ports that should actually be closed.
        2. Sensitive file exposure.
        3. Secrets exposed in page responses including API keys & passwords.
        4. Single request/response tests for XSS, SSRF, and RCE vulnerabilities.
        5. Path traversal.
        6. Exposure of data through internal software (e.g. monitoring).
        7. Fingerprinting technologies - will report on the software it discovers to help you stay on top of any changes in your domain, including rogue installations.
        8. Monitor subdomain for takeovers - Subdomains pointing to third party services no longer being used make it possible for malicious hackers to register the subdomain on that third party and (effectively) hijack the subdomain.

    i. Deep Scan

        1. Discover and fix the latest vulnerabilities with automated security tests from Detectify Crowdsource, an exclusive global network of top-ranked security researchers.
        2. Deep Scan uses real payloads rather than version testing, producing accurate scan results that cover a wide range of vulnerabilities not limited to CVE security issues.
        3. The web application is tested for 2000+ vulnerabilities, including OWASP Top 10 and CORS.
        4. New vulnerabilities are added to the scanner every day by our ethical hacker network.


   

